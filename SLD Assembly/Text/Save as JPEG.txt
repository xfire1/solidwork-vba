Dim swApp As Object
Sub main()
'get access to document
Dim swApp As SldWorks.SldWorks
Dim swmodel As SldWorks.ModelDoc2
Set swApp = Application.SldWorks
Set swmodel = swApp.ActiveDoc

'Get the model path
Dim strModelPath As String
strModelPath = swmodel.GetPathName

'replace SLDASM with JPEG
strModelPath = Replace(strModelPath, "SLDASM", "jpg")

'save Out Drawing
swmodel.Extension.SaveAs strModelPath, swSaveAsCurrentVersion, swSaveAsOptions_Silent, Nothing, Empty, Empty

End Sub



